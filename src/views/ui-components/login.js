import React, { useState, useEffect,useContext } from 'react';
import UserContext from "../../components/UserContext";
import {
    Col,
    Form,
    FormGroup,
    Label,
    Input,
    Button,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Row,
    Card,
    CardBody,
    CardHeader,
    FormText
} from 'reactstrap';

const Login = ({history}) => {
    const userContext = useContext(UserContext);
    const [passwordShow, setPasswordShow] = useState(false);
    const passwordVisiblity = () => {
        setPasswordShow(passwordShow ? false : true);
    };
    const doLogin = (event)=>{
        event.preventDefault();
        console.log("login add token here");
        userContext.doLogin(true);
        console.log(">>doLogin");
        history.push("/dashboard");
    }

    return (
        <div style={{ margin: "10%" }} >
            <Row>
                <Col sm={3}></Col>
                <Col sm={6}>
                    <Card className="shadow">
                        <CardHeader>
                            <center><h3>Login</h3></center>
                        </CardHeader>
                        <CardBody>
                            <Form>
                                <FormGroup>
                                    <img style={{ width: '80%' }}></img>
                                </FormGroup>
                                <FormGroup row>
                                    <Col sm={1}></Col>
                                    <Col sm={10}>
                                        <InputGroup>
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText><i className="fas fa-user"></i></InputGroupText>
                                            </InputGroupAddon>
                                            <Input type="text" name="email" placeholder="example@gmail.com" />
                                        </InputGroup>
                                    </Col>
                                    <Col sm={1}></Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col sm={1}></Col>
                                    <Col sm={10}>
                                        <InputGroup>
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText><i className="fas fa-key"></i></InputGroupText>
                                            </InputGroupAddon>
                                            <Input type={passwordShow ? "text" : "password"} name="password" placeholder="" />
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText><i onClick={passwordVisiblity} className="fa fa-eye"></i></InputGroupText>
                                            </InputGroupAddon>
                                        </InputGroup>
                                    </Col>
                                    <Col sm={1}></Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col sm={4}></Col>
                                    <Col sm={4}>
                                        <Button onClick={doLogin} className="btn btn-primary btn-block" color="primary">
                                            Login
                                    </Button>
                                    </Col>
                                    <Col sm={4}></Col>
                                </FormGroup>
                            </Form>
                        </CardBody>
                    </Card>
                </Col>
                <Col sm={3}></Col>
            </Row>
        </div>
    );
}
export default Login;
