import React, { useState, useEffect } from 'react';
import {
    Card,
    Popover,
    PopoverHeader,
    PopoverBody,
    CardBody,
    CardTitle,
    Button,
    Table,
    Input, InputGroup, InputGroupAddon, InputGroupText
} from 'reactstrap';

const Customers = (props) => {

    return (
        <div>
            <Card>
                <CardTitle className="bg-light border-bottom p-3 mb-0">
                    <i className="mdi mdi-account-multiple mr-2"></i>
            Customers
          </CardTitle>
                <CardBody className="">
                    <Table style={{ tableLayout: 'fixed' }} striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th style={{ textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>Name</th>
                                <th style={{ textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>Email</th>
                                <th style={{ textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>Phone</th>
                                <th style={{ textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style={{ textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>
                                    RAMA
                                </td>
                                <td style={{ textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>
                                    rama@gmail.com
                                </td>
                                <td style={{ textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>
                                    1234567890
                                </td>
                                <td style={{ textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>
                                    India
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </CardBody>
            </Card>
        </div>
    );
}

export default Customers;