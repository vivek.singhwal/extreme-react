import React from 'react';


const Footer = () => {
    return (
        <footer className="footer text-center">
            All Rights Reserved by Pyther. Designed and Developed by{' '}
            <a href="">Pyther Innovations Private Limited</a>.
        </footer>
    );
}
export default Footer;
