import Starter from '../views/starter/starter.js';
// ui components
import Customers from '../views/ui-components/customer';

var ThemeRoutes = [{
        path: '/dashboard',
        name: 'Dashboard',
        icon: 'ti-loop',
        component: Starter
    },
    {
        path: '/customers',
        name: 'Customers',
        icon: 'mdi mdi-account-multiple mr-2',
        component: Customers
    },
    { path: '/', pathTo: '/dashboard', name: 'Dashboard', redirect: true }
];
export default ThemeRoutes;